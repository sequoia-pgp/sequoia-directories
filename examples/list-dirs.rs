//! Dump a default configuration, and a non-default configuration.

use sequoia_directories::Home;
use sequoia_directories::Component;
use sequoia_directories::Result;

fn main() -> Result<()> {
    let dump = |title: &str, home: &Home| {
        println!();
        println!("{}", title);
        println!(
            "{}",
            std::iter::repeat("-")
                .take(title.chars().count())
                .collect::<Vec<_>>()
                .join(""));

        println!("home: {} (default: {})",
                 home.location().display(), home.is_default_location());
        println!("base config: {}",
                 home.base_config_dir().display());
        for (s, c) in [
            ("cert-d", Component::CertD),
            ("keystore", Component::Keystore),
            ("sq", Component::Sq),
            ("foo", Component::Other("foo".into())),
        ].iter()
        {
            println!("{}:", s);
            println!("  config dir: {}", home.config_dir(c).display());
            println!("  data dir: {}", home.data_dir(c).display());
            println!("  cache dir: {}", home.cache_dir(c).display());
        }
    };

    dump("Default configuration", &Home::new(None)?);

    let tmpdir = tempfile::tempdir()?;
    let tmpdir = tmpdir.path().to_path_buf();
    let home = Home::new(tmpdir.clone())?;
    dump(&format!("{}", tmpdir.display()), &home);

    Ok(())
}
