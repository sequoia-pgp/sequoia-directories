A small crate that returns the platform-specific configuration, data,
and cache directories.

This crate is primarily for use by Sequoia libraries, and applications
to locate their configuration, data, and cache directories.

By default, Sequoia uses the system's standard locations for user
data, configuration files, and cache data.  On Linux, for instance,
this means following [the XDG base directory specification].

  [the XDG base directory specification]: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

Sequoia programs also support using an alternate home directory.  In
this case, the user data, configuration files, and cache data are
placed under a single, unified directory.  This is a lightweight way
to partially isolate a program.

An alternate home location can be specified when instantiating a
`Home` object.  The user is also able to override the default by
setting the `SEQUOIA_HOME` environment variable.  The default home
location is the user's home directory.  On Linux, this usually looks
like `/home/USER`.

## Examples

Typical usage:

```rust
use sequoia_directories::Home;
use sequoia_directories::Component;
use sequoia_directories::Result;

fn main() -> Result<()> {
    // The user can still override this by setting the SEQUOIA_HOME
    // environment variable.
    let home = Home::new(None)?;
    println!("keystore's data directory: {}",
             home.data_dir(Component::Keystore).display());

    Ok(())
}
```

The directories on a Linux-based system:

```text
$ cargo run --example list-dirs | sed 's#/home/[^ /]*#/home/USER#; s#/tmp/[^ /]*#/tmp/TMPDIR#;'
Default configuration
---------------------
home: /home/USER (default: true)
base config: /home/USER/.config
cert-d:
  config dir: /home/USER/.config/pgp.cert.d
  data dir: /home/USER/.local/share/pgp.cert.d
  cache dir: /home/USER/.cache/pgp.cert.d
keystore:
  config dir: /home/USER/.config/sequoia/keystore
  data dir: /home/USER/.local/share/sequoia/keystore
  cache dir: /home/USER/.cache/sequoia/keystore
sq:
  config dir: /home/USER/.config/sequoia/sq
  data dir: /home/USER/.local/share/sequoia/sq
  cache dir: /home/USER/.cache/sequoia/sq
foo:
  config dir: /home/USER/.config/sequoia/foo
  data dir: /home/USER/.local/share/sequoia/foo
  cache dir: /home/USER/.cache/sequoia/foo

/tmp/TMPDIR
---------------
home: /tmp/TMPDIR (default: false)
base config: /tmp/TMPDIR
cert-d:
  config dir: /tmp/TMPDIR/config/pgp.cert.d
  data dir: /tmp/TMPDIR/data/pgp.cert.d
  cache dir: /tmp/TMPDIR/cache/pgp.cert.d
keystore:
  config dir: /tmp/TMPDIR/config/keystore
  data dir: /tmp/TMPDIR/data/keystore
  cache dir: /tmp/TMPDIR/cache/keystore
sq:
  config dir: /tmp/TMPDIR/config/sq
  data dir: /tmp/TMPDIR/data/sq
  cache dir: /tmp/TMPDIR/cache/sq
foo:
  config dir: /tmp/TMPDIR/config/foo
  data dir: /tmp/TMPDIR/data/foo
  cache dir: /tmp/TMPDIR/cache/foo

```

The directories on a Windows system (our CI):

```text
$ cargo run --example list-dirs
Default configuration
---------------------
home: C:\Users\ContainerAdministrator (default: true)
base config: C:\Users\ContainerAdministrator\AppData\Roaming
cert-d:
  config dir: C:\Users\ContainerAdministrator\AppData\Roaming\pgp.cert.d
  data dir: C:\Users\ContainerAdministrator\AppData\Roaming\pgp.cert.d
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\pgp.cert.d
keystore:
  config dir: C:\Users\ContainerAdministrator\AppData\Roaming\Sequoia-PGP\sequoia\config\keystore
  data dir: C:\Users\ContainerAdministrator\AppData\Roaming\Sequoia-PGP\sequoia\data\keystore
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Sequoia-PGP\sequoia\cache\keystore
sq:
  config dir: C:\Users\ContainerAdministrator\AppData\Roaming\Sequoia-PGP\sequoia\config\sq
  data dir: C:\Users\ContainerAdministrator\AppData\Roaming\Sequoia-PGP\sequoia\data\sq
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Sequoia-PGP\sequoia\cache\sq
foo:
  config dir: C:\Users\ContainerAdministrator\AppData\Roaming\Sequoia-PGP\sequoia\config\foo
  data dir: C:\Users\ContainerAdministrator\AppData\Roaming\Sequoia-PGP\sequoia\data\foo
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Sequoia-PGP\sequoia\cache\foo
C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly
-------------------------------------------------------------
home: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly (default: false)
base config: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly
cert-d:
  config dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\config\pgp.cert.d
  data dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\data\pgp.cert.d
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\cache\pgp.cert.d
keystore:
  config dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\config\keystore
  data dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\data\keystore
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\cache\keystore
sq:
  config dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\config\sq
  data dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\data\sq
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\cache\sq
foo:
  config dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\config\foo
  data dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\data\foo
  cache dir: C:\Users\ContainerAdministrator\AppData\Local\Temp\.tmpPrPmly\cache\foo
```
